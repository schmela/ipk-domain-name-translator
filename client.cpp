#include <iostream>
#include <string>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <regex.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "client.hpp"

const int MIN_INDEX = 0; //minimalni index akce
const int MAX_INDEX = 1; //maximalni index akce (jsou celkem 2 akce -4 -6)

const int UNDEFINED = -1;
const int FULLHEAD = 0;
const int IPV4 = 4; // -4 ipv4
const int IPV6 = 6; // -6 ipv6

using namespace std;

TParams::TParams(int argc,char *argv[])
{
	for (int i=MIN_INDEX; i<=MAX_INDEX; i++) {setAction(UNDEFINED,i);} 
	
	int action_index=MIN_INDEX;
	if ((argc==1)||(argc==3)) {throw badParameters();}
	else if (argc==2) {
		if (strcmp (argv[1],"-h") == 0)
		{
			cout <<	"CLIENT - preklad domenovych jmen na IPv4 a IPv6 adresy"<<endl<<
					"pouziti:"<<endl<<
					"client -h vypise tuto napovedu"<<endl<<
					"client [adresa_serveru:port] -46 [domenove_jmeno]"<<endl<<
					"na adrese_serveru musi na danem portu naslouchat serverova cast aplikace"<<endl<<
					"-4 prelozi zadane domenove jmeno na IPv4 adresu"<<endl<<
					"-6 prelozi zadane domenove jmeno na IPv6 adresu"<<endl<<flush;
			exit(EXIT_SUCCESS);
		}
		else
		{
			throw badParameters();
		}
	} 
	else {
		setDomainName(argv[argc-1]);
		//cout << "DomainName: "<<getDomainName()<<endl;
		splitHostName(argv[1]);
		int ipv4=0,ipv6=0; // pocty kolikrat ktery argumemt je
		for (int i=2; i<argc-1; i++)
		{
			if ((argv[i][0]!='-')||(strlen(argv[i])==1)) {throw badParameters();}
			
			// zjistovani co se bude delat v jakem poradi (ktery prepinac bude kolikaty)
			for (unsigned int j=1; j<strlen(argv[i]); j++)
			{
				if (argv[i][j]=='4')
					{ if (ipv4++==0) {setAction(IPV4,action_index++);}}
				else if (argv[i][j]=='6') 
					{ if (ipv6++==0) {setAction(IPV6,action_index++);}}
				else 
					{throw badParameters();}
			}
		}		
	}
}

void TParams::setAction(int type, int index)
{
	if ((index<MIN_INDEX) || (index>MAX_INDEX)) {throw badActionIndex();}
	actions[index]=type;
}

void TParams::setServer(string aAdress)
{
	server = aAdress;
}


void TParams::setPort(int aPort)
{
	port = aPort;
}

void TParams::setDomainName(string aName)
{
	domainName = aName;
}

// rozdeli prvni parametr na adresu serveru(pripadne ji prelozi) a port
void TParams::splitHostName(string aHostName)
{
	string adress,port_,file_;
	// ziskani samotne adresy
	//if ( (adress = regexp(aHostName,"^http[:]//([^/]+)")) != "")
	if ( (adress = regexp(aHostName,"^([^:]+)")) != "")
	{
		//cout << "Adresa: " <<adress<< endl;
		// kdyz je v adrese i port, tak se vezme z ni, kdyz ne, program skonci
		if ( (port_ = regexp(aHostName,".:([[:digit:]]+)")) != "")
		{
			setPort(atoi(port_.c_str()));
			//cout << "Client: Port: " << getPort()<<endl<<flush;
		}
		else {throw errorMissingPort();}
	}
	else {throw badHostName();}

	setServer(adress);
	//cout << "Client: Server-address: " << getServer()<<endl<<flush;
}

int TParams::getAction(int index)
{
	if ((index<MIN_INDEX) || (index>MAX_INDEX)) {throw badActionIndex();}
	return actions[index];
}

string TParams::getServer()
{
	return server;
}

int TParams::getPort()
{
	return port;
}

string TParams::getDomainName()
{
	return domainName;
}

void dnslookup (int ipVersion, string adress, int port, string request)
{
	char addrstr[INET_ADDRSTRLEN];
    struct addrinfo hints, *res, *p;
    int status;
	void *ptr=NULL;

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET; // AF_INET or AF_INET6, jinak AF_UNSPEC
	hints.ai_socktype = SOCK_STREAM;

	// intToStr port
    char buf[5];
    sprintf(buf,"%d",port);
    char *port_ = (char*) &buf;

	// ziskavani ip adresy serveru
	if ((status = getaddrinfo(adress.c_str(), port_,&hints, &res)) != 0) 
		{throw getaddrinfoError();}
	
	// projizdi se vsechny nalezene vysledky a vezme se prvni IPv4 adresa
	while (res)
	{
		if (res->ai_family == AF_INET)
		{
			ptr=&((struct sockaddr_in *) res->ai_addr)->sin_addr;
			break;
		}
		res = res->ai_next;
	}

	// kdyz se nenasla ani jedna -> konec
	if (ptr==NULL)
	{
		freeaddrinfo(res);
		throw getaddrinfoError();
	}

	inet_ntop(res->ai_family, ptr, addrstr, INET_ADDRSTRLEN);
	//cout << "Client: Translated-address: "<<addrstr<<endl<<flush;

	// stvoreni socketu 
	int sockfd;
	if (( sockfd = socket(res->ai_family, res->ai_socktype, 0)) < 0){
		freeaddrinfo(res);
		throw socketError();
	}
	//cout << "Client: Socket - OK"<<endl<<flush;

	// pripojeni k serveru 
	if ( connect(sockfd, res->ai_addr ,res->ai_addrlen) < 0){
		freeaddrinfo(res);
		throw connectError();
	}
	//cout << "Client: Connect - OK"<<endl<<flush;
	
	// dealokace
	freeaddrinfo(res);

	// poslani pozadavku
	string message = ((ipVersion==IPV4)? "4 " : "6 ");
	message.append(request);
	if ( write(sockfd, message.c_str(),message.size()) < 0)
		{throw writeError();}
	//cout << "Client: Wrote message "<< message <<endl<<flush;
  
	// prijmuti zpravy 		  
	char buffer2[4096]={0};
	if ( read(sockfd, buffer2, 4096) < 0 )
		{throw readError();}
	if (strcmp(buffer2,"Nenalezeno")==0)
		cerr << "Err"<<ipVersion<<": Nenalezeno."<<endl<<flush;
	else
		//cout << "Client: Read message "<< buffer2 <<endl<<flush;
		cout << buffer2 << endl <<flush;
	
	// zavreni spojeni
	if ( close(sockfd) < 0 )
		{throw closeError();}
	//cout << "Client: Connection closed - OK"<<endl<<flush;
}

// vrati obsah prvni zavorky z regexpu, kdyz neni nalezena, vraci se ""
string regexp(string source, string pattern)
{
    regex_t regex;
	int c;

	// kompilace regulerniho vyrazu
	try
	{
    	c = regcomp(&regex, pattern.c_str(), REG_EXTENDED);
		if ( c != 0 ) {throw regexpCompilationError();}
	}
	catch (regexpCompilationError) 
	{
		cerr << "Regex compilation Error" << endl;
		regfree(&regex);
		exit ( EXIT_FAILURE );
	}

	// vykonani regulerniho vyrazu
	size_t     nmatch = 2;
	regmatch_t pmatch[2];
	if ((regexec(&regex,source.c_str(),nmatch,pmatch,0)) == 0)
	{
		int begin = (int)pmatch[1].rm_so;
		int end = (int)pmatch[1].rm_eo;
		int length = end-begin;
		regfree(&regex);
		if ((begin==-1)||(end==-1)) return "";
		else return source.substr(begin,length); 
	}
	else
	{
		// kdyz se nic nenaslo, vraci se prazdny retezec
		regfree(&regex);
		return "";
	}
}

int main(int argc,char *argv[]) 
{
	try
	{
		TParams parametry(argc,argv);

		for (int i=MIN_INDEX; i<=MAX_INDEX; i++) // i - index akce
		{
			switch (parametry.getAction(i))
		  	{
				case UNDEFINED:
					break;
				case IPV4:
					dnslookup(IPV4,parametry.getServer(),parametry.getPort(),parametry.getDomainName());
				  	break;
				case IPV6: 
					dnslookup(IPV6,parametry.getServer(),parametry.getPort(),parametry.getDomainName());
				  	break;
				default:
					return EXIT_FAILURE;        
		  	}
		}
	}

	catch (TParams::badParameters)
	{
		cerr << "Spatne parametry" << endl;
		return EXIT_FAILURE;
	}
	
	catch (TParams::badActionIndex)
	{
		cerr << "Spatny index pro akci" << endl;
		return EXIT_FAILURE;
	}

	catch (TParams::badHostName)
	{
		cerr << "Spatne zadana adresa" << endl;
		return EXIT_FAILURE;
	}
	
	catch (TParams::errorMissingPort)
	{
		cerr << "Chybejici informace o portu" << endl;
		return EXIT_FAILURE;
	}

	catch (socketError)
	{
		cerr << "Socket error" << endl;
		return EXIT_FAILURE;
	}
	
	catch (connectError)
	{
		cerr << "Connect error" << endl;
		return EXIT_FAILURE;
	}

	catch (writeError)
	{
		cerr << "Write Error" << endl;
		return EXIT_FAILURE;
	}
	
	catch (readError)
	{
		cerr << "Read Error" << endl;
		return EXIT_FAILURE;
	}
	
	catch (closeError)
	{
		cerr << "Close Error" << endl;
		return EXIT_FAILURE;
	}

	catch (getaddrinfoError)
	{
		cerr << "getaddrinfo Error" << endl;
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}
