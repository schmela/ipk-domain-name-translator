CC=g++
CPPFLAGS=-std=c++98 -pedantic -Wextra -W -g

all: client server

client: client.cpp client.hpp
	$(CC) client.cpp $(CPPFLAGS) -o client

server: server.cpp server.hpp
	$(CC) server.cpp $(CPPFLAGS) -o server

pack: 
	tar -czf xhrade08.tar.gz Makefile server.cpp server.hpp client.cpp client.hpp xhrade08.pdf
