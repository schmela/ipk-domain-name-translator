#include <iostream>
#include <string>

using namespace std;

class TParams
{
	private:
		string server; // adresa serveru
		string domainName; // prekladane jmeno
		int actions[2]; // v jakem poradi jde ktery parametr
		int port; // port na komunikaci se serverem


		void setAction(int type, int index);
		void setServer(string aAdress);
		void setPort(int aPort);
		void setDomainName(string aName);
		void splitHostName(string aHostName);

	public:
		TParams(int argc,char *argv[]);
		int getAction(int index);
		string getServer();
		int getPort();
		string getDomainName();
		
		class badParameters{};
		class badActionIndex{};
		class badHostName{};
		class errorMissingPort{};
};
void dnslookup(int ipVersion, string adress, int port, string request);
class getaddrinfoError{};
class socketError{};
class connectError{};
class writeError{};
class readError{};
class closeError{};        

string regexp(string source, string pattern);
class regexpCompilationError{};
