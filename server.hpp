#include <iostream>
#include <string>

using namespace std;

string dnslookup(int ipVersion, string address);

class badParameters{};
class badPort{};
class badHostName{};
class badIpVersion{};
class badMessage{};

class getaddrinfoError{};
class socketError{};
class setSockOptError{};
class bindError{};
class connectError{};
class writeError{};
class readError{};
class closeError{};        
class forkError{};
class listenError{};
