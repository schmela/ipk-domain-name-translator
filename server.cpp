#include <iostream>
#include <string>
#include <vector>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <regex.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "server.hpp"
#include <signal.h>
#include <sys/wait.h>
#include <math.h>

using namespace std;

const int MIN_INDEX = 0; //minimalni index akce
const int MAX_INDEX = 1; //maximalni index akce (jsou celkem 2 akce -4 -6)

const int UNDEFINED = -1;
const int FULLHEAD = 0;
const int IPV4 = 4; // -4 ipv4
const int IPV6 = 6; // -6 ipv6

const int LISTEN_PORT = 10000;

// odchytavani signalu pro rodice a potomka
struct sigaction signaly, signaly_p;

// pole kde jsou ulozeni vsichni potomci
std::vector<pid_t> pid_potomci(0);

// sockety pro rodice a pro potomka
int socket_potomek=0, socket_rodic=0;

// obsluzna funkce pro signaly prijate rodicem
void sig_rodic (int sig)
{
	switch (sig)
	{
		case SIGINT: ;
		case SIGTERM:
			{
				// cout << "SIGTERM, killing all children"<<endl<<flush;
				for (unsigned int i=0; i<=(pid_potomci.size()); i++)
				{
					if (pid_potomci[i]!=0)
					{
						kill(pid_potomci[i],SIGTERM);
						int status;
						waitpid(pid_potomci[i],&status,0);
						// printf("pid %d killed\n",pid_potomci[i]);
					}
				}
				close(socket_potomek);
				close(socket_rodic);
				exit (EXIT_SUCCESS);
				break;
			}
		case SIGCHLD:
			{
				int status;
				wait(&status);
				// cout << "Dite umrelo"<<endl<<flush;
				break;
			}
		default:;
	}
}

// obsluzna funkce pro signaly prijate detmi
void sig_potomek (int sig)
{
	switch (sig)
	{
		case SIGINT: ;
		case SIGTERM:
			{
				// cout << "SIGTERM, child killed."<<endl<<flush;
				close(socket_potomek);
				exit(EXIT_SUCCESS);
				break;
			}
		default:;
	}
}

// provede preklad domenoveho jmena address na IP adresu verze ipVersion
string dnslookup (int ipVersion, string address)
{
    struct addrinfo hints, *res, *p;
	void *ptr=NULL;
	char addrstr[100];

	memset(&hints, 0, sizeof(hints));
	switch (ipVersion)
	{
		case IPV4:
			hints.ai_family = AF_INET; // AF_INET or AF_INET6, jinak AF_UNSPEC
			break;
		case IPV6:
			hints.ai_family = AF_INET6; 
			break;
		default:
			throw badIpVersion();
	}
	hints.ai_socktype = SOCK_STREAM;
 
	// samotny preklad 
	if ((getaddrinfo(address.c_str(), NULL, &hints, &res)) != 0) 
	{
		// cout << "Child: Not Found"<<endl<<flush;
		//freeaddrinfo(res);
		return "";
	}

	// prochazime vsechny vysledky a vybirame prvni co odpovida zvolene verzi IP
	while (res)
	{
		if ((res->ai_family == AF_INET) && (ipVersion==IPV4))
		{
          	ptr = &((struct sockaddr_in *) res->ai_addr)->sin_addr;
          	break;
		}
		else if ((res->ai_family == AF_INET6) && (ipVersion==IPV6))
		{
          	ptr = &((struct sockaddr_in6 *) res->ai_addr)->sin6_addr;
          	break;
		}

		res = res->ai_next;
	}

	if (ptr==NULL)
		{throw getaddrinfoError();}

	// prevedeni adresy do citelneho tvaru
    inet_ntop (res->ai_family, ptr, addrstr, 100);
	// cout << "Child: Resolved-adress: "<<addrstr<<endl<<flush;

	freeaddrinfo(res);
	return addrstr;
}

int main(int argc,char *argv[]) 
{
	try
	{
		// obsluha signalu pro rodice
		signaly.sa_flags = 0;
		sigemptyset (&signaly.sa_mask);
		sigaddset(&signaly.sa_mask,SIGINT);
		sigaddset(&signaly.sa_mask,SIGCHLD);
		sigaddset(&signaly.sa_mask,SIGTERM);
		signaly.sa_handler = sig_rodic;
		sigaction (SIGTERM, &signaly, 0);
		sigaction (SIGINT, &signaly, 0);
		sigaction (SIGCHLD, &signaly, 0);

		// kontrola spravnosti parametru
		if (argc != 3) {throw badParameters();}

		if (strcmp(argv[1],"-p")!=0) {throw badParameters();}

		// socket pro potomka
		struct sockaddr_in sa_potomek;

		// vytvoreni socketu pro rodice
		//int socket_rodic;
		if ((socket_rodic = socket(PF_INET, SOCK_STREAM, 0)) < 0)
			{throw socketError();}
		// cout << endl<<"Server: Socket - OK"<<endl<<flush;

		struct sockaddr_in sa_rodic;
		memset(&sa_rodic,0,sizeof(sa_rodic));
		sa_rodic.sin_family = AF_INET;
		sa_rodic.sin_addr.s_addr = INADDR_ANY;

		// kontrola spravnosti portu
		int port;
		if ((sscanf(argv[2],"%d",&port)!=1) || (fabs( (double)port - strtod(argv[2],NULL) != 0.0)))
			{throw badPort();}

		// navazani portu s rodicem
		sa_rodic.sin_port = htons(port);

		// resi problem s tim, ze socket po killu zustane jeste chvilku viset v systemu
		int yes=1;
		if (setsockopt(socket_rodic,SOL_SOCKET,SO_REUSEADDR,&yes,sizeof(int)) == -1) 
			{throw setSockOptError();}

		// navazani socketu na lokalni port
		int rc;
		if ((rc = bind(socket_rodic, (struct sockaddr*)&sa_rodic, sizeof(sa_rodic))) < 0)
			{	cerr << "chyba:"<<rc << endl << flush;
				throw bindError();}
		// cout << "Server: Bind - OK"<<endl<<flush;

		// pasivni otevreni
		if ((rc = listen(socket_rodic, 1)) < 0)
			{throw listenError();}
		// cout << "Server: Listen - OK"<<endl<<flush;

		int i=0;
		while (1)
		{
			//cout << "Server: waiting for connection..."<<endl<<flush;
			// cekani na zadost o spojeni
			socklen_t sa_potomek_len = sizeof(sa_potomek);
			socket_potomek = accept(socket_rodic, (struct sockaddr*)&sa_potomek, &sa_potomek_len);	
			if (socket_potomek <= 0)
				continue;
			//cout << "Server: Accept - OK"<<endl<<flush;

			pid_potomci[i]=fork();
			if ( pid_potomci[i]==-1 ) 
			{
				// fork se nepovedl -> zavrit vsechny potomky
				//
				// znici vsecny potomky
				for (int j=0; j < i;j++)
				{
					int status;
					kill(pid_potomci[j],SIGTERM);
					waitpid (pid_potomci[j],&status,0);
				}
				
				// zavre socket pro potomka
				if ( close(socket_potomek) < 0)
					{throw closeError();}
				// zavre socket pro rodice
				if ( close(socket_rodic) < 0)
					{throw closeError();}
				throw forkError();
			}
			else 
				if ( pid_potomci[i]==0 )
				{
					// jsme v potomkovi

					// ukonci se socket_rodic, ten potrebuje jen rodic
					if ( close(socket_rodic) < 0)
						{throw closeError();}

					//cout << "Child["<< i<<"] created - OK" <<endl<<flush;

					// chytani signalu pro potomka
					signaly_p.sa_flags = 0;
					sigemptyset (&signaly_p.sa_mask);
					sigaddset(&signaly_p.sa_mask,SIGINT);
					sigaddset(&signaly_p.sa_mask,SIGTERM);
					signaly_p.sa_handler = sig_potomek;
					sigaction (SIGTERM, &signaly_p, 0);
					sigaction (SIGINT, &signaly_p, 0);
					
					// simulace dlouhotrvajiciho potomka
					/*
					int i;
					while (1)
					{
						i++;
					}
					*/
				
					

					// read - nacteni toho co chce klient resolvovat a jak
					char buffer2[4096]={0};
					if ( read(socket_potomek, buffer2, 4096) < 0 )
						{throw readError();}
					//cout << "Child[" << i << "] prijal " << buffer2 << endl<<flush;
					
					// se stringem se pracuje lepe a radostneji
					string buf=buffer2;
					
					// prijata zprava -> port + domainname
					int verzeIP;
					switch (buf[0])
					{
						case '4':
							verzeIP = IPV4;
							break;
						case '6':
							verzeIP = IPV6;
							break;
						default:
							throw badMessage();
					}
					
					// vytazeni domenoveho jmena pro preklad
					string domainname=buf.substr(2,buf.length()-2);
					
					// samotne prelozeni dns adresy
					string odpoved = dnslookup (verzeIP, domainname);
					if (odpoved.empty())
					{
						odpoved = "Nenalezeno";
					}
					
					// poslani zpravy zpet klientovi
					if ( write(socket_potomek, odpoved.c_str(),odpoved.size()) < 0)
						{throw writeError();}	
					//cout << "Child[" << i << "] answered " << odpoved << endl<<flush;

					// zavreni socketu pro potomka
					if ( close(socket_potomek) < 0)
						{throw closeError();}

					return EXIT_SUCCESS;

				}
				else 
				{
					// jsme v rodici
					// cout << "Server: Fork - OK"<<endl<<flush;
					
					// okamzite zavreni socketu pro potomka
					if ( close(socket_potomek) < 0) 
						{throw closeError();}

					// zvedne se index potomku
					i++;
				}
		}
	}

	catch (badParameters)
	{
		cerr << "Spatne zadane parametry." << endl << flush;
		return EXIT_FAILURE;
	}

	catch (badIpVersion)
	{
		cerr << "Spatne verze IP protokolu." << endl << flush;
		return EXIT_FAILURE;
	}

	catch (badMessage)
	{
		cerr << "Zprava prisla ve spatnem formatu." << endl << flush;
		return EXIT_FAILURE;
	}

	catch (badPort)
	{
		cerr << "Spatne zadany port." << endl << flush;
		return EXIT_FAILURE;
	}

	catch (getaddrinfoError)
	{
		cerr << "getaddrinfo Error." << endl << flush;
		return EXIT_FAILURE;
	}

	catch (socketError)
	{
		cerr << "Socket error." << endl << flush;
		return EXIT_FAILURE;
	}

	catch (bindError)
	{
		cerr << "Bind Error." << endl << flush;
		return EXIT_FAILURE;
	}
	
	catch (setSockOptError)
	{
		cerr << "setSockOpt Error." << endl << flush;
		return EXIT_FAILURE;
	}

	catch (writeError)
	{
		cerr << "Write Error." << endl << flush;
		return EXIT_FAILURE;
	}
	
	catch (readError)
	{
		cerr << "Read Error." << endl << flush;
		return EXIT_FAILURE;
	}
	
	catch (closeError)
	{
		cerr << "Close Error." << endl << flush;
		return EXIT_FAILURE;
	}

	catch (forkError)
	{
		cerr << "Fork Error." << endl << flush;
		return EXIT_FAILURE;
	}

	catch (listenError)
	{
		cerr << "Listen Error." << endl << flush;
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
