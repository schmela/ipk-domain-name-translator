#!/bin/sh

if [ "x"$1 = "x" ];then
	echo "Set port as argument u n00b !!!"
	exit 1
fi

RUN_IT()
{
	me=`whoami`
	tmp1=`mktemp -t $me`
	tmp2=`mktemp -t $me`
	echo "$@"
	ret=`echo -n $?`
	$@ > $tmp1 2>$tmp2
	echo "- STDOUT:"
	cat $tmp1
	echo "- STDERR:"
	cat $tmp2
	echo "- Return code: $ret"
	echo "--------------"
	rm "$tmp1"
	rm "$tmp2"
}

echo "[ Compiling your crappy programs ]"
make

echo
echo "***************"

echo "[ Starting server on port $1 ]"
./server -p $1 >server.stdout 2>server.stderr & 

echo

echo "***************"
echo "[ Basic tests ]"

RUN_IT "./client localhost:$1 -4 -6 fituska.eu"
RUN_IT "./client localhost:$1 -6 -4 fituska.eu"
RUN_IT "./client localhost:$1 -4 fituska.eu"
RUN_IT "./client localhost:$1 -6 fituska.eu"
RUN_IT "./client localhost:$1 -6 -4 google.com"
RUN_IT "./client localhost:$1 -4 -6 google.com"
RUN_IT "./client localhost:$1 -4 google.com"
RUN_IT "./client localhost:$1 -6 google.com"
RUN_IT "./client localhost:$1 -6 -4 ipv6.google.com"
RUN_IT "./client localhost:$1 -4 -6 ipv6.google.com"
RUN_IT "./client localhost:$1 -4 ipv6.google.com"
RUN_IT "./client localhost:$1 -6 ipv6.google.com"

echo "***************"
echo "[ How \"ps\" looks like??? ]"
ps | grep "./server"

echo

echo "***************"
echo "[ Advanced tests ]"

for i in `seq 1 20`
do
	echo -n "."
	RUN_IT "./client localhost:$1 -4 -6 google.com" > /dev/null &
done
echo

echo "***************"
sec=5
echo "[ Let's sleep for $sec seconds. That should be enough to get responses... ]"
sleep $sec

echo 
echo "***************"
echo "[ How \"ps\" looks like now??? ]"
ps | grep "./server"

echo
echo "***************"

echo "[ Kill your shitty server ]"
killall server
echo

echo "***************"
echo "[ Sleep for few seconds - give time to your slow program to finally shut down ]"
sleep 5

echo 
echo "***************"

echo "[ How \"ps\" looks like now??? ]"
ps | grep "./server"
